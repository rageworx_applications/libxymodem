﻿#ifndef __LIBXYMODEM_H__
#define __LIBXYMODEM_H__
/*******************************************************************************
** libxymodem, (C)2017 Raph.K
** for linux or Mac OS-X.
*******************************************************************************/

class XYMODEMEvent
{
	public:
		virtual void OnConnected() = 0;
		virtual void OnDisconnected() = 0;
		virtual void OnWriteBuffer( unsigned current, unsigned total ) = 0;
		virtual void OnError( int errid, const char* errmsg ) = 0;
};

class XYMODEM
{
    public:
        typedef enum
        {
            PROTOCOL_X = 0,
            PROTOCOL_Y
        }ProtocolType;

    public:
        XYMODEM( ProtocolType ptype = PROTOCOL_X, XYMODEMEvent* eh = NULL );
        ~XYMODEM();

    public:
        bool Open( const char* devname = NULL, unsigned baudrate = 0 );
        void Close();

    public:
        /***
        * State returns :
        *    -1  = Closed.
        *     0  = Opened.
        *     0 >= States : 'N' == Nacknowledge
        *                   '.' == Acknowledge
        *                   '?' == Unknwon
        ***/
        int  State()        { return current_state; }
        void Sync( bool e ) { waitforsync = e; }
        bool Sync()         { return waitforsync; }

    public:
        long long WriteRawBuffer( const char* buffer, unsigned bufferlen );
        long long WriteFile( const char* filename = NULL );
        long long WriteBuffer( const char* buffer, unsigned bufferlen, const char* refname = NULL );
        unsigned  GetReceiveSize();
        unsigned  Receive( char* buff, unsigned sz );

	public:
		int			CurrentFD()		{ return current_fd; }
		const char* ErrorString() 	{ return errorstring; }
		int         ErrorID()     	{ return errorID; }

    private:
        char*           current_devname;
        int             current_state;
        int             current_fd;
        unsigned        current_baudrate;
        ProtocolType    current_protocol;
        bool            waitforsync;
		XYMODEMEvent*   eventhandler;
		char*			errorstring;
		int             errorID;
};
#endif /// of __LIBXYMODEM_H__
