﻿#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>

#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#define TTY_READSIZE_IOCTL

#ifdef TTY_READSIZE_IOCTL
#include <sys/ioctl.h>
#endif

#include <sysexits.h>
#include <sys/param.h>
#include <sys/select.h>
#include <sys/time.h>

#include <CoreFoundation/CoreFoundation.h>
#include <IOKit/IOKitLib.h>
#include <IOKit/serial/IOSerialKeys.h>
#include <IOKit/serial/ioss.h>
#include <IOKit/IOBSD.h>

#include "xymodem.h"

////////////////////////////////////////////////////////////////////////////////

#define X_STX 0x02
#define X_ACK 0x06
#define X_NAK 0x15
#define X_EOF 0x04

#define min(a, b)       ((a) < (b) ? (a) : (b))
#define POLYCRC16       0x1021
#define CHUNKSZ         1024

#define MAX_ERRSTR_LEN	1024
#define ERRSTR(...)		if ( errorstring != NULL ) { sprintf( errorstring, __VA_ARGS__ ); }
#define ERRSTR_CLEAR()	if ( errorstring != NULL ) { memset( errorstring, 0, MAX_ERRSTR_LEN ); }
#define ERRORID( _X_ )  errorID = _X_;
#define REPORTERR()		if ( eventhandler != NULL ) { eventhandler->OnError( errorID, errorstring ); }

////////////////////////////////////////////////////////////////////////////////

struct XM_P_CHUNK
{       uint8_t     start;
        uint8_t     block;
        uint8_t     block_neg;
        uint8_t     payload[CHUNKSZ];
        uint16_t    crc16;
} __attribute__((packed));

////////////////////////////////////////////////////////////////////////////////

static uint16_t crc16_update( uint16_t crc_in, bool increase )
{
    uint16_t xor_v = ( crc_in & ( 0xFFFE) ) >> 15;
    uint16_t out_v = ( crc_in & ( 0x7FFF) ) << 1;

    if ( increase == true )
    {
        out_v++;
    }

    if ( xor_v > 0 )
    {
        out_v ^= POLYCRC16;
    }

    return out_v;
}

static uint16_t crc16( const uint8_t *data, uint16_t size )
{
    uint16_t cnt = 0;

    for ( cnt=0; size>0; size--, data++ )
    {
        for ( uint16_t rpt=0x80; rpt>0; rpt>>=1 )
        {
            bool toggle = false;

            if ( ( *data & rpt ) > 0 )
            {
                toggle = true;
            }

            cnt = crc16_update( cnt, toggle );
        }
    }

    for ( uint16_t rpt=0; rpt<16; rpt++ )
    {
        cnt = crc16_update( cnt, false );
    }

    return cnt;
}

static uint16_t swap16( uint16_t in )
{
    return ( ( in & 0xFF00 ) >> 8) | ( ( in & 0x00FF ) << 8 );
}

////////////////////////////////////////////////////////////////////////////////

XYMODEM::XYMODEM( ProtocolType ptype, XYMODEMEvent* eh )
 : current_devname( NULL ),
   current_state( -1 ),
   current_fd( -1 ),
   current_baudrate( 0 ),
   current_protocol( ptype ),
   eventhandler( eh ),
   errorstring( NULL )
{
	errorstring = new char[ MAX_ERRSTR_LEN ];

	ERRSTR_CLEAR();
}

XYMODEM::~XYMODEM()
{
    Close();
}

bool XYMODEM::Open( const char* devname, unsigned baudrate )
{
	ERRSTR_CLEAR();
	ERRORID( 0 );

    if ( devname == NULL )
    {
		ERRSTR( "no device name" );
		return false;
	}

    // Check device existed.
    if ( access( devname, F_OK ) != 0 )
    {
		ERRSTR( "device %s not existed", devname );
		return false;
	}

    if ( current_devname != NULL )
    {
        free( current_devname );
        current_devname = NULL;
    }

    current_devname = strdup( devname );

    if ( current_fd >= 0 )
        Close();

	// int o_attr = O_RDWR | O_NOCTTY | O_NDELAY;
	int o_attr = O_RDWR | O_NOCTTY | O_NONBLOCK;
	
    current_fd = open( current_devname, o_attr );
    
    if ( current_fd < 0 )
    {
        ERRSTR( "open device failure : %s", current_devname );
		ERRORID( -errno );
        return -errno;
    }
	
	if ( ioctl( current_fd, TIOCEXCL ) == -1 )
	{
		ERRSTR( "TIOEXECL returns -1" );
		ERRORID( -errno );
		return -errno;
	}
	
    // Open the device in nonblocking mode
    fcntl( current_fd, F_SETFL, FNDELAY );
	// fcntl( current_fd, F_SETFL, 0 );

    struct termios tio = {0,};

    tcgetattr( current_fd, & tio );
	cfmakeraw( &tio );
	
    //bzero( &tio, sizeof(tio) );
	
    // Configure the device : 8 bits, no parity, no control
    tio.c_cflag |= ( CLOCAL | CREAD | CS8 );     
    tio.c_iflag |= ( IGNPAR | IGNBRK );

	// Removu may need these:
	tio.c_cflag |= ( CCTS_OFLOW | /// CTS on
					 CRTS_IFLOW );  /// RTS ib

    // Timer unused
    tio.c_cc[VTIME] = 0;
    // At least on character before satisfy reading
    tio.c_cc[VMIN] = 2;
    	
    speed_t spd;
    
    switch( baudrate )
    {
        case 110  :     spd = B110; break;
        case 300  :     spd = B300; break;
        case 600  :     spd = B600; break;
        case 1200 :     spd = B1200; break;
        case 2400 :     spd = B2400; break;
        case 4800 :     spd = B4800; break;
        case 9600 :     spd = B9600; break;
        case 19200 :    spd = B19200; break;
        case 38400 :    spd = B38400; break;
        case 57600 :    spd = B57600; break;

        default:
        case 115200 :   spd = B115200; break;
    }

    cfsetispeed( &tio, spd );
    cfsetospeed( &tio, spd );
    
    // Activate the settings    
    tcsetattr( current_fd, TCSANOW, &tio );

    // Assert Data Terminal Ready (DTR)
    if ( ioctl( current_fd, TIOCSDTR ) == -1) 
	{
		ERRSTR( "TIOEXECL returns -1" );
		ERRORID( -errno );
		return -errno;
    }

    // Clear Data Terminal Ready (DTR)
    ioctl( current_fd, TIOCCDTR );

    // Set the modem lines depending on the bits set in handshake
    int handshake = TIOCM_DTR | TIOCM_RTS | TIOCM_CTS | TIOCM_DSR;

    ioctl( current_fd, TIOCMSET, &handshake );
	
    // Store the state of the modem lines in handshake

    ioctl(current_fd, TIOCMGET, &handshake);

#if 0
    unsigned long mics = 1UL;

    // set latency to 1 microsecond
    ioctl(current_fd, IOSSDATALAT, &mics);
#endif
	
    current_state = 0;

	if ( eventhandler != NULL )
	{
		eventhandler->OnConnected();
	}

    return true;
}

void XYMODEM::Close()
{
    if( current_fd >= 0 )
    {
        close( current_fd );
        current_fd = -1;
    }

    if ( current_devname != NULL )
    {
        free( current_devname );
        current_devname = NULL;
    }

    current_baudrate = 0;
    current_state = -1;

	if ( eventhandler != NULL )
	{
		eventhandler->OnDisconnected();
	}
}

long long XYMODEM::WriteRawBuffer( const char* buffer, unsigned bufferlen )
{
    if ( buffer == NULL )
    {
        return -1;
    }

    size_t len = (size_t)bufferlen;
    size_t que = 0;

    while ( len > 0 )
    {
        int ssz = CHUNKSZ;
        if ( ( que + ssz ) > bufferlen )
        {
            ssz = bufferlen - que;
        }

        int ret = write( current_fd, &buffer[que], ssz );

        if ( ret != ssz )
            return -errno;

        len -= ret;
        que += ret;
    }

    return bufferlen;
}

long long XYMODEM::WriteFile( const char* filename )
{
	ERRSTR_CLEAR();
	ERRORID( 0 );

    if ( current_fd < 0 )
    {
		ERRSTR( "device not open" );
		REPORTERR();
		return 0;
	}

    if ( filename == NULL )
	{
		ERRSTR( "filename not decided" );
		REPORTERR();
		return 0;
	}

    int f_fd = open( filename, O_RDONLY );
    if ( f_fd < 0 )
    {
        ERRSTR( "File open failure : %s", filename );
		ERRORID( -errno );
		REPORTERR();
        return -errno;
    }

    struct stat fdstat = {0,};

    fstat( f_fd, &fdstat );

    size_t len = fdstat.st_size;

    const uint8_t* buff = (const uint8_t*) mmap( NULL,
                                                 len,
                                                 PROT_READ,
                                                 MAP_PRIVATE,
                                                 f_fd,
                                                 0 );
    if ( buff == NULL )
    {
        ERRSTR( "mmap() failure" );
		ERRORID( -errno );
		REPORTERR();
        return -errno;
    }

    return WriteBuffer( (const char*)buff, (unsigned)len, filename );
}

long long XYMODEM::WriteBuffer( const char* buffer, unsigned bufferlen, const char* refname )
{
	ERRSTR_CLEAR();
	ERRORID( 0 );

    if ( buffer == NULL )
    {
		ERRSTR( "buffer is empty " );
        return -1;
    }

    uint8_t answer = 0;
    size_t  len = (size_t)bufferlen;

    if ( waitforsync == true )
    {
        // Waiting for receiver ping ...
        do
        {
            int ret = read( current_fd, &answer, sizeof(answer) );

            if ( ret != sizeof(answer) )
            {
                ERRSTR( "read() failure for waitforsync" );
				ERRORID( -errno );
				REPORTERR();
                return -errno;
            }
        }
        while ( answer != 'C' );
    }

    int skip_payload = 0;
    struct XM_P_CHUNK chunk = {0,};

    if ( current_protocol == XYMODEM::PROTOCOL_Y )
    {
        if ( refname != NULL )
        {
            strncpy( (char *) chunk.payload,
                     refname,
                     sizeof(chunk.payload) );
        }
        else
        {
            strncpy( (char *) chunk.payload,
                     "nonamed",
                     sizeof(chunk.payload) );
        }

        chunk.block = 0;
        skip_payload = 1;
    }
    else
    {
        chunk.block = 1;
    }

    unsigned char status = 0x00;
    chunk.start = X_STX;

    while ( len > 0 )
    {
        size_t z = 0;
        int next = 0;

        if ( skip_payload == 0 )
        {
            z = min( len, sizeof(chunk.payload) );
            memcpy( chunk.payload, buffer, z );
            memset( chunk.payload + z, 0xFF, sizeof(chunk.payload) - z );
        }
        else
        {
            skip_payload = 0;
        }

        chunk.crc16 = swap16( crc16( chunk.payload, sizeof(chunk.payload) ) );
        chunk.block_neg = 0xFF - chunk.block;

        int ret = write( current_fd, &chunk, sizeof(chunk) );

        if ( ret != sizeof(chunk) )
		{
			ERRSTR( "write failure!" );
			ERRORID( -errno );
			REPORTERR();
			return -errno;
		}

        ret = read( current_fd, &answer, sizeof(answer) );

        if ( ret != sizeof(answer) )
                return -errno;

        switch (answer)
        {
            case X_NAK:
                    status = 'N';
                    break;

            case X_ACK:
                    status = '.';
                    next = 1;
					if ( eventhandler != NULL )
					{
						eventhandler->OnWriteBuffer( len, bufferlen );
					}
                    break;

            default:
                    status = '?';
                    break;
        }

        current_state = (int)status;

        if ( next > 0 )
        {
            chunk.block++;
            len -= z;
            buffer += z;
        }
    }

    uint8_t eof = X_EOF;

    int ret = write( current_fd, &eof, sizeof(eof) );

    if ( ret != sizeof(eof) )
    {
		ERRSTR( "%s", strerror( errno ) );
		ERRORID( -errno );
		REPORTERR();
		return -errno;
	}

    // Send EOT again for YMODEM
    if ( current_protocol == XYMODEM::PROTOCOL_Y )
    {
        ret = write( current_fd, &eof, sizeof(eof) );

        if (ret != sizeof(eof))
		{
			ERRSTR( "%s", strerror( errno ) );
			ERRORID( -errno );
			REPORTERR();
			return -errno;
		}
    }

    return bufferlen;
}

unsigned XYMODEM::GetReceiveSize()
{
    if ( current_fd >= 0 )
    {
#ifdef TTY_READSIZE_IOCTL
        int rsz = 0;
        
        if ( ioctl( current_fd, FIONREAD, &rsz ) == 0 )
		{
        	return (unsigned)rsz;
		}

		return 0;
#else
        struct stat fdstat = {0,};

        fstat( current_fd, &fdstat );

        return fdstat.st_size;
#endif /// of __APPLE__
    }

    return 0;
}

unsigned XYMODEM::Receive( char* buff, unsigned sz )
{
    if ( current_fd < 0 )
        return 0;

#ifdef __APPLE__
    size_t rsz = 0;
    
    rsz = read( current_fd, buff, sz );
    if ( rsz > 0 )
    {
        return (unsigned)rsz;
    }

    return 0;
#else
    unsigned que = 0;

    for (;;)
    {
        char in = 0;

        size_t rsz = read( current_fd, &in, sizeof(char) );

        buff[ que ] = in;

        que++;

        if ( que >= sz )
            return que;
    }

	return que;
#endif /// of __APPLE__
}
