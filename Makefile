# Makefile for libxymodem
# (C)2017 Raphael Kim / rageworx@gmail.com
#

#########################################################################
# About cross compiler, or other platform :
#
# To enable build for embedded linux, you may encomment next 2 lines.
# Or, may need to change your cross compiler path with environments.
# It is up to developer !

# CCPREPATH = ${ARM_LINUX_GCC_PATH}
# CCPREFIX  = arm-linux-

# To enable build for embedded linux, change following line.
# CCPATH    = ${CCPREPATH}/${CCPREFIX}
CCPATH =
#########################################################################

# Compiler configure.
GCC = ${CCPATH}gcc
GPP = ${CCPATH}g++
AR  = ${CCPATH}ar

# Sources and how it built
# Optimization issue: recommend to build with using -ffast-math option.
# Change if your CPU architecture supports more high technologies.
INCDIR    = ./inc
SOURCEDIR = ./src
OBJDIR    = ./obj/Release
OUTBIN    = libxymodem.a
OUTDIR    = ./lib
DEFINEOPT =
CFLAGS    = -std=c++11
OPTIMIZEOPT = -O3 -s
CPUARCHOPT  =

ifeq (debug,$(firstword $(MAKECMDGOALS)))
	DEFINEOPT += -DDEBUG
	OPTIMIZEOPT =
	OUTBIN = libxymodem_d.a
endif

CFLAGS += -I$(INCDIR) -I$(SOURCEDIR) -I$(FLTKDIR) $(DEFINEOPT) $(OPTIMIZEOPT) $(CPUARCHOPT) $(BITSOPT)

.PHONY: 32bit debug

all: prepare clean ${OUTDIR}/${OUTBIN}

32bit: all
debug: all

prepare:
	@mkdir -p ${OBJDIR}
	@mkdir -p ${OUTDIR}

${OBJDIR}/xymodem.o:
	@$(GPP) -c ${SOURCEDIR}/xymodem.cpp ${CFLAGS} -o $@

${OUTDIR}/${OUTBIN}: ${OBJDIR}/xymodem.o 
	@echo "Generating $@ ..."
	@$(AR) -q $@ ${OBJDIR}/*.o
	@cp ${SOURCEDIR}/xymodem.h ${OUTDIR}

clean:
	@echo "Cleaning built directories ..."
	@rm -rf ${OBJDIR}/*
	@rm -rf ${OUTDIR}/${OUTBIN}
